#!/usr/bin/env bash
name="$(cat content/._Name.meta)"
if [[ ! $name ]]; then
    echo "Name metadata not set!"
    exit 1
fi
