package v4

import (
	_ "gitlab.com/rackn/provision/v4/cli"
	_ "github.com/stevenroose/remarshal"
)

var (
	RSMajorVersion = "4"
	RSMinorVersion = "0"
	RSPatchVersion = "0"
	RSExtra        = "-pre"
	BuildStamp     = "Not Set"
	RSVersion      = "v" + RSMajorVersion + "." + RSMinorVersion + "." + RSPatchVersion + RSExtra
)
